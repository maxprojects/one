require 'test_helper'

class SitesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get blog" do
    get :blog
    assert_response :success
  end

  test "should get music_finder" do
    get :music_finder
    assert_response :success
  end

  test "should get document_organizer" do
    get :document_organizer
    assert_response :success
  end

  test "should get professional_services" do
    get :professional_services
    assert_response :success
  end

end
